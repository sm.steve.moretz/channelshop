<?php

use App\Http\Middleware\AlreadyHasValidSubscription;
use App\Http\Middleware\AlreadyHasValidSubscriptionApi;
use App\Http\Middleware\HasValidSubscription;
use App\Http\Middleware\HasValidSubscriptionApi;
use App\Http\Middleware\IsAdmin;
use App\Shopping;
use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::get('signup/activate/{token}', 'AuthController@signupActivate');
    Route::post('sendCode', 'AuthController@sendcode');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
    Route::post('resetByCode', 'PasswordResetController@resetByCode');
});

Route::group([
    'prefix' => 'comment'
],function(){
    Route::post('create','CommentsController@create')->middleware('auth:api');
    Route::post('confirm/{comment}','CommentsController@confirm')->middleware('auth:api',IsAdmin::class);
    Route::post('destroy/{comment}','CommentsController@destroy')->middleware('auth:api',IsAdmin::class);
    Route::get('indexUser','CommentsController@indexUser');
    Route::get('indexAdmin','CommentsController@indexAdmin')->middleware('auth:api',IsAdmin::class);
});

Route::group([
    'prefix' => 'user'
],function(){
    Route::get('profile','ProfileController@index')->middleware('auth:api');
});

Route::group([
    'prefix' => 'testimontional'
],function(){
    Route::post('create','TestimontionalController@create')->middleware('auth:api',IsAdmin::class);
    Route::post('update/{testimontional}','TestimontionalController@update')->middleware('auth:api',IsAdmin::class);
    Route::post('destroy/{testimontional}','TestimontionalController@destroy')->middleware('auth:api',IsAdmin::class);
    Route::get('indexUser','TestimontionalController@indexUser');
    Route::get('indexAdmin','TestimontionalController@indexAdmin')->middleware('auth:api',IsAdmin::class);
});

Route::group([
    'prefix' => 'subscription'
],function(){
    Route::post('add/{duration}','PaymentController@payWithpaypal')->middleware('auth:api','verified');
    Route::post('check/{user}','SubscriptionController@check')->middleware('auth:api',IsAdmin::class);
    Route::get('getNotChecked','SubscriptionController@getNotChecked')->middleware('auth:api',IsAdmin::class);
});


//Route::post('/', function () {
//    return Auth::user();
//})->middleware('auth:api','verified');

Route::post('test', function(Request $request) {
//    Auth::user()->addSubscription(6);
    return Auth::user();
})->middleware('auth:api');
//Route::post('test', function(Request $request) {
//    return auth()->guard('api')->user() == null ? 'not' : 'yes' ;
//});
