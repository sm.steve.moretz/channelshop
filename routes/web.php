<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', function () {
    return view('home');
});


Route::get('/pay', function () {
    return view('pay');
});

Route::get('/', function () {

//    dd(env('MAIL_PASSWORD'));
    $data = array('name'=>"Virat Gandhi");

    $send = Mail::send(['text' => 'mail'], $data, function ($message) {
        $message->to('sm.steve.moretz@gmail.com', 'Tutorials Point')->subject
        ('Laravel Basic Testing Mail');
        $message->from('xyz@gmail.com', 'Virat Gandhi');
    });
    dd($send);
    echo "Basic Email Sent. Check your inbox.";
    dd('Up N Running');
});

Route::get('/status','PaymentController@verifyPaypal')->name('status');

//Route::group([
//    'middleware' => [IsAdmin::class],
//    'prefix' => 'admin-panel'
//], function () {
//
//});
//
//Route::get('/ws', function (Request $request) {
//    dispatch(new RatchetSimpleMessageJob(RatchetEntity::sendMessage(1,['test'=>'lol','ygn'=>'love'],true)));
//    return view('ws',[
//        'token' => TokenAuth::createAndSaveTokenByLoggedInWeb($request)
//    ]);
//})->middleware('auth');
