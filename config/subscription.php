<?php

return [
    'one-month'=>[
        'price' => 10,
        'duration_months' => 1
    ],
    'three-months'=>[
        'price' => 30,
        'duration_months' => 3
    ],
    'six-months'=>[
        'price' => 60,
        'duration_months' => 6
    ],
    'one-year'=>[
        'price' => 120,
        'duration_months' => 12
    ]
];
