<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\News
 *
 * @property int $id
 * @property string $title
 * @property string $link
 * @property string $html
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $summary
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereSummary($value)
 * @property int $isEnglish
 * @method static \Illuminate\Database\Eloquent\Builder|\App\News whereIsEnglish($value)
 */
class News extends Model
{
    protected $casts = [
        'updated_at' => 'datetime:Y-m-d H:i',
        'time' => 'datetime:Y-m-d H:i',
    ];
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();
        static::creating(static function(News $news){
            $news->html = clean($news->html);
        });
        static::deleted(static function(News $news){
            $path = 'img/news/' . ($news->id) . '.png';
            if(\File::exists($path)){
                unlink($path);
            }
        });
    }
}
