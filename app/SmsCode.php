<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\SmsCode
 *
 * @property int $id
 * @property string $code
 * @property string $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsCode wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SmsCode whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SmsCode extends Model
{
    protected $guarded = [];

    public static function removeExpiredCodes(){
        self::where(['created_at','>',Carbon::now()->subMinutes(5)]);
    }
}
