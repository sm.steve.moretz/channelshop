<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RatchetUserPolicy
 *
 * @property int $blocker
 * @property int $blocked
 * @property int $allower
 * @property int $allowed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetUserPolicy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetUserPolicy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetUserPolicy query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetUserPolicy whereAllowed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetUserPolicy whereAllower($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetUserPolicy whereBlocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetUserPolicy whereBlocker($value)
 * @mixin \Eloquent
 */
class RatchetUserPolicy extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    public static function canSendFromTo($mode = 'allow',$fromUserId,$toUserId){
        if($mode === 'allow'){//default is allow so check blocker and blocked
            return !static::where([['blocked',$toUserId],['blocked',$fromUserId]])->exists();
        }else{//default is block so check allower and allowed
            return static::where([['allower',$toUserId],['allowed',$fromUserId]])->exists();
        }
    }
}
