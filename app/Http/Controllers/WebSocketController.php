<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use Ratchet\ConnectionInterface;

class WebSocketController extends WebSocketControllerBase
{
    public function onMessage(ConnectionInterface $conn, $msg)
    {
        $data = parent::onMessage($conn, $msg);
        if (!$data) return;
        if (isset($data->command)) {
            switch ($data->command) {

            }
        }
    }

    function onSingleMessage($isSavedInDB, $data, $from, $to)
    {
        if (isset($data->method) && $data->method === 'order') {
            return false;
        }
        if ($from === 1 || $to === 1) {
            $chat = Chat::create([
                'from_id' => $from,
                'to_id' => $to,
                'message' => $data->msg
            ]);
            $chat = Chat::where('id', $chat->id)->selectRaw('*,pdate(CONVERT_TZ(updated_at,"+00:00","' . env('timeOffset') . '")) as time')->get()[0];
            $chat->fromUser = User::find($from);
            $chat->method = 'chat';
//            $chat->delete();
            echo json_encode($chat);
            return $chat;
        }
        return '';

    }

    function onTopicMessage($isSavedInDB, $data, $from, $to)
    {
        echo json_encode(['type' => 'topic', 'isSaved' => $isSavedInDB, 'd' => $data]) . "\n";
    }
}
