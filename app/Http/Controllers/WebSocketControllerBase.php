<?php

namespace App\Http\Controllers;

use App\PushTemp;
use App\RatchetOnlineUser;
use App\RatchetTopic;
use App\RatchetUserPolicy;
use App\User;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\TokenAuth;

interface HelloW{
    function onTest();
}

abstract class WebSocketControllerBase implements MessageComponentInterface
{
    private $users;
    private $phpClients;
    public function __construct()
    {
        RatchetOnlineUser::truncate();
        $this->users = [];
        $this->phpClients = [];
    }
    /**
     * [onOpen description]
     * @method onOpen
     * @param  ConnectionInterface $conn [description]
     * @return [JSON]                    [description]
     * @example connection               var conn = new WebSocket('ws://localhost:8090');
     */
    public function onOpen(ConnectionInterface $conn)
    {
        echo "Connected {$conn->resourceId}\n";
    }

    public function sendAndRemoveUserCapturedMessages(ConnectionInterface $conn){
        if(isset($this->users[$conn->resourceId])){
            $pushes = PushTemp::where('receiverId', RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'));
            foreach ($pushes->get() as $push){
                $message = json_decode($push->message,true);
                if(isset($message['to'])){unset($message['to']);}
                if(isset($message['command'])){unset($message['command']);}
                if(isset($message['topic'])){unset($message['topic']);}
                if(isset($message['persist'])){unset($message['persist']);}
                $conn->send(json_encode(['payload'=>$message]));
            }
            $pushes->delete();
        }
    }


    public function isUser(ConnectionInterface $conn){
        return isset($this->users[$conn->resourceId]);
    }

    public function isAdmin(ConnectionInterface $conn){
        return $this->isUser($conn) && User::find(RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'))->isAdmin();
    }

    public function isPhpClient(ConnectionInterface $conn){
        return isset($this->phpClients[$conn->resourceId]);
    }


    public function checkUserPolicy(ConnectionInterface $conn,$toUserId){
        $fromUserId = RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id');
        if($this->isPhpClient($conn)){
            return env('WS_Policy_phpClient.to.toAllUsers') === 'allow';
        }

        if($this->isAdmin($conn)){
            return env('WS_Policy_admin.to.allUsers') === 'allow';
        }

        if($this->isUser($conn)){
            return RatchetUserPolicy::canSendFromTo(env('WS_Policy_user.to.user'),$fromUserId,$toUserId);
        }
    }

    public function checkSendToTopicPolicy(ConnectionInterface $conn,$toTopic){
        $fromUserId = RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id');
        $policy1 = RatchetTopic::canSendToTopic(env('WS_Policy_user.to.topic'),$toTopic,$fromUserId);
        $policy2 = (env('WS_Policy_subscriber.to.subscribedTopic') === 'allow' && RatchetTopic::where([['user_id',$fromUserId],['topic_id',$toTopic]])->exists());//user is a subscriber of the topic and wants to send to that topic.Like a group chat
        return $policy1 && $policy2;
    }

    abstract function onSingleMessage($isSavedInDB,$data,$from,$to);
    abstract function onTopicMessage($isSavedInDB,$data,$from,$to);

    /**
     * [onMessage description]
     * @method onMessage
     * @param  ConnectionInterface $conn [description]
     * @param  [JSON.stringify]              $msg  [description]
     * @return [JSON]                    [description]
     * @example subscribe                conn.send(JSON.stringify({command: "subscribe", channel: "global"}));
     * @example groupchat                conn.send(JSON.stringify({command: "groupchat", message: "hello glob", channel: "global"}));
     * @example message                  conn.send(JSON.stringify({command: "message", to: "1", from: "9", message: "it needs xss protection"}));
     * @example register                 conn.send(JSON.stringify({command: "register", userId: 9}));
     */
    public function onMessage(ConnectionInterface $conn, $msg)
    {
        echo 'onMessage -> ';
        echo $msg.PHP_EOL;
        $data = json_decode($msg);
        if (isset($data->command)) {
            switch ($data->command) {
                case "auth_php_client":
                    if($data->WS_PHP_CLIENT_KEY === env('WS_PHP_CLIENT_KEY')){
                        $this->phpClients[$conn->resourceId] = $conn;
                        echo "Php Client " .$conn->resourceId. " has been attached\n";
                    }
                    break;
                case "auth":
                    $userId = TokenAuth::getUserIdByToken($data->token);
                    $conn->send($userId ?json_encode(['Authenticated'=>true,'userId'=>$userId]): json_encode(['Authenticated'=>false]));
                    if($userId){
                        RatchetOnlineUser::create(['conn_id'=>$conn->resourceId,'user_id'=>$userId]);
                        $this->users[$conn->resourceId] = $conn;
                        $this->sendAndRemoveUserCapturedMessages($conn);
                    }
                    return false;
                case "subscribe":
//                    RatchetTopic::addToTopic($data->topic,RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'));
                    return false;
                case "messageToTopic":
                    $topicName = $data->topic;
                    if(!$this->checkSendToTopicPolicy($conn,$topicName)){
                        echo "User has no Permission to send message to topic : ".$data->topic . "\n";
                        return false;
                    }
                    unset($data->command, $data->topic);
                    $persist = false;
                    if(isset($data->persist)){
                        $persist = $data->persist;
                        unset($data->persist);
                    }
                    foreach(RatchetTopic::where('topic_id', $topicName)->get() as $topic){
                        $user = RatchetOnlineUser::findByUserId($topic->user_id);
                        if($user->exists()){//user is online
                            if($user->value('user_id') === RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'))continue;
                            $toConnectionIds = RatchetOnlineUser::findByUserId($topic->user_id)->get();
                            foreach ($toConnectionIds as $toConnectionId){
                                $this->users[$toConnectionId->conn_id]->send(json_encode(['payload'=>$data]));
                            }
                            $this->onTopicMessage(true,$data,RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'),$topic->user_id);
                        }else if($persist){//user is offline put to database if needed
                            $to = $topic->user_id;
                            PushTemp::create([
                                'senderId'=>RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'),
                                'receiverId'=>$to,
                                'message'=>json_encode($data)
                            ]);
                            $this->onTopicMessage(true,$data,RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'),$to);
                        }
                    }
                    return false;
                case "message":
                    $allowed = $this->checkUserPolicy($conn, $data->to);
                    if(!$allowed){
                        $conn->send(json_encode(['command'=>'message','status'=>false,'message'=> 'no permission']));
                        echo "User has no Permission to send message to userId : ".$data->to . "\n";
                        return false;
                    }
                    $persist = false;
                    if(isset($data->persist)){
                        $persist = $data->persist;
                        unset($data->persist);
                    }
                    if($data->to === RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id')){//sendMessageToSelf
                        $conn->send(json_encode(['command'=>'message','status'=>false,'message'=> 'send to self']));
                    }else if(RatchetOnlineUser::findByUserId($data->to)->exists()){//user is online
                        $to = $data->to;
                        $toConnectionIds = RatchetOnlineUser::findByUserId($data->to)->get();
                        unset($data->command,$data->to);
                        $consumed = $this->onSingleMessage(false,$data,RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'),$to);
                        foreach ($toConnectionIds as $toConnectionId){
                            if($consumed)$data = $consumed;
                            $this->users[$toConnectionId->conn_id]->send(json_encode(['payload'=>$data]));
                        }
                        $conn->send(json_encode(['payload'=>$data]));
                        $conn->send(json_encode(['command'=>'message','status'=>true,'viaDB'=> false]));
                    }else if($persist){
                        $to = $data->to;
                        $consumed = $this->onSingleMessage(true,$data,RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'),$to);
                        if($consumed)$data = $consumed;
                        PushTemp::create([
                            'senderId'=>RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'),
                            'receiverId'=>$to,
                            'message'=>json_encode($data)
                        ]);
                        $conn->send(json_encode(['command'=>'message','status'=>true,'viaDB'=> true]));
                    }else{
                        $to = $data->to;
                        $consumed = $this->onSingleMessage(false,$data,RatchetOnlineUser::findByConnectionId($conn->resourceId)->value('user_id'),$to);
                        if($consumed)$data = $consumed;
                        $conn->send(json_encode(['payload'=>$data]));
                        $conn->send(json_encode(['command'=>'message','status'=>false,'message'=> 'offline user']));
                    }
                    return false;
                default:
                    return $data;
            }
        }
    }
    public function onClose(ConnectionInterface $conn)
    {
        RatchetOnlineUser::findByConnectionId($conn->resourceId)->delete();
        if(isset($this->users[$conn->resourceId])) {
            unset($this->users[$conn->resourceId]);
            echo "User Client {$conn->resourceId} has disconnected\n";
        }else if(isset($this->phpClients[$conn->resourceId])) {
            unset($this->phpClients[$conn->resourceId]);
            echo "Php Client " .$conn->resourceId. " has been dettached\n";
        }else{
            echo "Connection {$conn->resourceId} has disconnected\n";
        }
    }
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}
