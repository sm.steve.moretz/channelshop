<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Routing\Controller;

class ProfileController extends Controller
{
    public function index(){
        return Auth::user();
    }
}
