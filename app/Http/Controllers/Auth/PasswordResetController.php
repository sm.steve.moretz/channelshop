<?php

namespace App\Http\Controllers\Auth;

use App\SmsCode;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\User;
use App\PasswordReset;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{


    public function resetByCode(){
        SmsCode::removeExpiredCodes();
        $validated = \request()->validate([
            'code'=>'required|string|exists:sms_codes',
            'phone'=>'required|string|exists:sms_codes',
            'newPassword' => 'required|string'
        ]);
        $smsCode = SmsCode::where([['code', $validated['code']], ['phone', $validated['phone']]])->first();
        $user = User::where('phone', $smsCode->phone)->first();
        $user->password = Hash::make($validated['newPassword']);
        $user->update();
        try {
            $smsCode->delete();
        } catch (\Exception $e) {
        }
        return ['status' => 'ok','message'=>'Successfully Reset Password!'];
    }

    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $registerViaPhone = env('REGISTER_VIA_PHONE', false);
        $request->validate([
//            'email' => 'required|string|email',
//            'phone' => 'required',
            $registerViaPhone ? 'phone' : 'email' => $registerViaPhone ? 'required' : 'required|string|email'
        ]);
//        $user = User::where('email', $request->email)->first();
        $user = User::where($registerViaPhone ? 'phone' : 'email', $registerViaPhone ? $request->phone : $request->email)->first();
        if (!$user)
            return response()->json([
//                'message' => "We can't find a user with that e-mail address."
                'message' => "We can't find a user with that phone number."
            ], 404);
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $registerViaPhone ? $request->phone : $request->email],
            [
                'email' => $registerViaPhone ? $request->phone : $request->email,
                'token' => Str::random(60)
            ]
        );
        if ($user && $passwordReset) {
            $url = url('/api/password/find/' . $passwordReset->token);
//            return $url;
        }
        if($registerViaPhone){
            //TODO send sms
        }else{
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
        }
        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return abort(404);
//            return response()->json(['message' => 'This password reset token is invalid.'], 404);
        }
        return \Redirect::to(env('REDIRECT_FOR_RESET')."?token={$passwordReset->token}&email={$passwordReset->email}");
//        return view('auth.passwords.reset', [
//            'api' => true,
//            'token' => $passwordReset->token,
//            'email' => $passwordReset->email,
//            'errors' => null
//        ]);
    }

    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $request->validate([
//            'email' => 'required|string|email',
            'email' => 'required|string',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return response()->json([
                'message' => "We can't find a user with that e-mail address."
            ], 404);
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
//        $user->notify(new PasswordResetSuccess($passwordReset));
//        return response()->json($user);
        return json_encode(['status'=>'با موفقیت انجام شد']);
    }
}
