<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Comment
 *
 * @property int $id
 * @property string $comment
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUserId($value)
 * @mixin \Eloquent
 * @property int $has_confirmed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereHasConfirmed($value)
 * @property string|null $username
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUsername($value)
 */
class Comment extends Model
{
    protected $guarded = [];
    public static function getNotConfirmed(){
        $collection = Comment::where('has_confirmed', false)->get(['id','comment', 'username', 'user_id']);
        foreach($collection as $item){
            if($item->user_id !== 1){
                $item->username = User::find($item->user_id)->name;
            }
            unset($item['user_id']);
        }
        return $collection;
    }
    public static function getIndexForUser(){
        $collection = Comment::orderBy('created_at', 'desc')->where('has_confirmed',true)->take(10)->get(['comment', 'username', 'user_id']);
        foreach($collection as $item){
            if($item->user_id !== 1){
                $item->username = User::find($item->user_id)->name;
            }
            unset($item['user_id']);
        }
        return $collection;
    }
}
