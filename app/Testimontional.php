<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Testimontional
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimontional newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimontional newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimontional query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimontional whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimontional whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimontional whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimontional whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Testimontional whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Testimontional extends Model
{
    protected $guarded = [];
}
