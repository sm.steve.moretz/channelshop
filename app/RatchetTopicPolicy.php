<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RatchetTopicPolicy
 *
 * @property int $topic
 * @property int $blocked
 * @property int $allowed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopicPolicy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopicPolicy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopicPolicy query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopicPolicy whereAllowed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopicPolicy whereBlocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopicPolicy whereTopic($value)
 * @mixin \Eloquent
 */
class RatchetTopicPolicy extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
