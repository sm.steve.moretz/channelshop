<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RatchetTopic
 *
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopic whereUserId($value)
 * @mixin \Eloquent
 * @property string $topic_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetTopic whereTopicId($value)
 */
class RatchetTopic extends Model
{
    protected $guarded = [];

    public static function addToTopic($topic,$userId){
        static::updateOrCreate(['topic_id'=>$topic,'user_id'=>$userId],['topic_id'=>$topic,'user_id'=>$userId]);
    }

    public static function canSendToTopic($mode = 'allow',$topic,$fromUserId){
        if($mode === 'allow'){
            return !RatchetTopicPolicy::where([['topic',$topic],['blocked',$fromUserId]])->exists();
        }else{
            return RatchetTopicPolicy::where([['topic',$topic],['allowed',$fromUserId]])->exists();
        }
    }
}
