<?php

namespace App\Services\BootstrapTable;

use App\Products;
use App\shoppings;
use COMPLEX_TABLE;

class BootstrapTable
{
    /**
     * @param $table
     */
    public static function handle($table,$searchColumn)
    {
        try {
            $r = new \ReflectionClass($table);
            $instance =  $r->newInstanceWithoutConstructor();
            if(request('search')){
                $search = request('search');
                $instance = $instance->where($searchColumn,'LIKE',"%$search%");
            }
            $count = $instance->count();
            if(request('order') && request('sort')){
                $instance = $instance->orderBy(request('sort'),request('order'));
            }
            if(request('limit')){
                $offset = request('offset') ?: 0;
                $limit = request('limit');
                $instance = $instance->skip($offset)->take($limit);
            }
            echo json_encode(['total'=>$count,'rows'=>$instance->get()]);
        } catch (\ReflectionException $e) {
            echo 'error';
        }
    }

    public static function handleModel($instance,$searchColumn)
    {
        try {
            if(request('search')){
                $search = request('search');
                $instance = $instance->where($searchColumn,'LIKE',"%$search%");
            }
            $count = $instance->count();
            if(request('order') && request('sort')){
                $instance = $instance->orderBy(request('sort'),request('order'));
            }
            if(request('limit')){
                $offset = request('offset') ?: 0;
                $limit = request('limit');
                $instance = $instance->skip($offset)->take($limit);
            }
            echo json_encode(['total'=>$count,'rows'=>$instance->get()]);
        } catch (\ReflectionException $e) {
            echo 'error';
        }
    }

    public static function getTableId(){
        return request('table_id');
    }
}
