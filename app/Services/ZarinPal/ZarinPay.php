<?php
namespace App\Services\ZarinPal;
use App\ZarinPayTemp;
use Carbon\Carbon;

class ZarinPay{
    public static function putInTempPay($json){
        return ZarinPayTemp::create(['json'=>$json])->id;
    }
    public static function getPayloadAndDelete($id){
        $collection = ZarinPayTemp::findOrFail($id);
        $payload = json_decode($collection->json, true);
        $collection->delete();
        return $payload;
    }
    public function getInTempPay(){
        request()->validate([
            'oId' => ['required']
        ]);
        return ZarinPayTemp::findOrFail(request('oId'));
    }
    public function removeTempPay(){
        ZarinPayTemp::findOrFail(request('oId'))->delete();
    }
    function pay($amount,$description,$payJsonObject,$email = 'nomail@gmail.com',$mobile = '0'){
        ZarinPayTemp::whereRaw('updated_at + INTERVAL 20 MINUTE <"' . Carbon::now('UTC') . '"')->delete();
        $Amount = $amount; //Amount will be based on Toman
        $Description = $description; // Required
        $payJsonObject += ['price' => $amount];
        $CallbackURL = env('ZARIN_CALLBACK_URL').'?oId='.$this->putInTempPay($payJsonObject);
        $client = new \SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $client->PaymentRequest(
            [
                'MerchantID' => env('ZARIN_MERCHANTID'),
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $email,
                'Mobile' => $mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );
        if ($result->Status == 100) {
//        Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority);
            //برای استفاده از زرین گیت باید ادرس به صورت زیر تغییر کند:
            Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority.'/ZarinGate');
            dd('end');
        } else {
            echo'ERR: '.$result->Status;
        }
    }
    public function verify(){
        $validated = \request()->validate([
            'Status'=>['required'],
            'Authority'=>['required']
        ]);
        if ($_GET['Status'] == 'OK') {
            $client = new \SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            $result = $client->PaymentVerification(
                [
                    'MerchantID' => env("ZARIN_MERCHANTID"),
                    'Authority' => request('Authority'),
                    'Amount' => json_decode(($this->getInTempPay()->json),true)['price'],
                ]
            );

            if ($result->Status == 100) {
                $inTempPay = $this->getInTempPay();
                $this->removeTempPay();
                return $inTempPay;//ok
            } else {
                return false;//error
            }
        } else {
            return null;//cancel
        }
    }
}
