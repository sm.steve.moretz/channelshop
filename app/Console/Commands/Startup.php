<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class Startup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'starter:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Do all needed at the start of the project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('migrate');
        echo Artisan::output();
        Artisan::call('passport:install', ['--force' => true ]);
        echo Artisan::output();
    }
}
