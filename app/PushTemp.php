<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PushTemp
 *
 * @property int $senderId
 * @property int $receiverId
 * @property mixed $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PushTemp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PushTemp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PushTemp query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PushTemp whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PushTemp whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PushTemp whereReceiverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PushTemp whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PushTemp whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PushTemp extends Model
{
    protected $guarded = [];
}
