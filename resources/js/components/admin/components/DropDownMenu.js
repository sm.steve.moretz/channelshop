import PropTypes from 'prop-types'
import React from 'react';

const DropDownMenu = props => {
    return (
        <li className="nav-item dropdown">
            <a className="nav-link" data-toggle="dropdown" href="#">
                <i className={`fa ${props.icon}`}></i>
                {props.badge > 0 && <span className="badge badge-danger navbar-badge">{props.badge}</span>}
            </a>
            <div className="dropdown-menu dropdown-menu-lg dropdown-menu-left">
                {props.children}
            </div>
        </li>
    )
};

export default DropDownMenu;

DropDownMenu.propTypes = {
  badge: PropTypes.any,
  children: PropTypes.any,
  icon: PropTypes.any
}
