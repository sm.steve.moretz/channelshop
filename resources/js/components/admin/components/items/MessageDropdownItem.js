import PropTypes from 'prop-types'
import React, {useEffect, useState} from 'react';
import moment from "moment/moment";
const MessageDropdownItem = props => {
    const [newTime, setNewTime] = useState(moment(props.time).fromNow());
    useEffect(()=>{
        let intervalId = setInterval(()=>{
            setNewTime(moment(props.time).fromNow());
        },5000);
        return ()=>{
            clearInterval(intervalId);
        }
    },[]);
    return (
        <div>
            <a href="#" className="dropdown-item">
                {/* Message Start */}
                <div className="media">
                    <img src={props.picture} alt="User Avatar"
                         className="img-size-50 ml-3 img-circle"></img>
                    <div className="media-body">
                        <h3 className="dropdown-item-title">
                            <span className={`float-left text-sm text-${props.starColor}`}><i
                                className="fa fa-star"></i></span>
                        </h3>
                        <p className="text-sm">{props.title}</p>
                        <p className="text-sm text-muted"><i className="fa fa-clock-o mr-1"></i>{newTime}</p>
                    </div>
                </div>
                {/* Message End */}
            </a>
            <div className="dropdown-divider"></div>
        </div>
    )
};

export default MessageDropdownItem;

MessageDropdownItem.propTypes = {
  picture: PropTypes.any,
  starColor: PropTypes.any,
  time: PropTypes.any,
  title: PropTypes.any
}
