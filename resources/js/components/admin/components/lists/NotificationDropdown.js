import PropTypes from 'prop-types'
import React from 'react';

const NotificationDropdown = props => {
    return (
        <div className="dropdown-menu-lg dropdown-menu-left">
            {props.header && <span className="dropdown-item dropdown-header">{props.header}</span>}
            <div className="dropdown-divider"></div>
            {props.children}
            {props.footer && <a href={props.footerLink} className="dropdown-item dropdown-footer">{props.footer}</a>}
        </div>
    )
};

export default NotificationDropdown;

NotificationDropdown.propTypes = {
  children: PropTypes.any,
  footer: PropTypes.any,
  footerLink: PropTypes.any,
  header: PropTypes.any
}
