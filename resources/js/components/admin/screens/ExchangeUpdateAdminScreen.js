import React, {useCallback, useEffect, useRef, useState} from 'react';
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from '@material-ui/core/styles';
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Form from "../../../modules/Form";
import InputAdornment from "@material-ui/core/InputAdornment";
import AttachMoney from "@material-ui/icons/esm/AttachMoney";
import Fab from "@material-ui/core/Fab";
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Snackbar from "@material-ui/core/Snackbar";
import {useDispatch, useSelector} from "react-redux";
import {addAllExchange, addExchange, removeExchange} from "../redux/exchangeAction";
import fetchServer from "../../../modules/fetchServer";
import 'react-dropzone-uploader/dist/styles.css';
import {Env} from "../../../env";
import UploaderComponent from "../../../modules/UploaderComponent";
import ReactImageFallback from "react-image-fallback";

const useStyles = makeStyles(theme => ({
    container: {
        display: 'grid',
        gridTemplateColumns: 'repeat(12, 1fr)',
        gridGap: theme.spacing(3),
    },
    test: {
        backgroundColor: 'red'
    },
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 150,
        },
        flexGrow: 1,
    },
    card: {
        margin: 10
    }
}));


const validate = (name, text) => {
    if (text.length > 100) {
        return 'خیلی طولانی است';
    }
    if(text.length === 0){
        return 'این فیلد باید پر شود'
    }
    return true;
};

export default function CSSGrid(props) {
    const exchanges = useSelector(state => state.exchange.listOfExchanges);
    const dispatch = useDispatch();
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const [snackOpen, setSnackOpen] = useState(false);
    const [snackText, setSnackText] = useState('آپدیت شد');
    const [updater, setUpdater] = useState();
    const deleteIdRef = useRef();
    const closeDialog = () => {
        setOpen(false);
    };
    const handleCloseSnackbar = () => {
        setSnackOpen(false)
    };
    const handleDelete = async () => {
        var res = await fetchServer(`admin-panel/exchange/delete/${deleteIdRef.current}`, 'POST');
        try {
            var json = await res.json();
            dispatch(removeExchange(json.id));
        }catch (e) {}
        closeDialog();
    };

    useEffect(()=>{
        dispatch(addAllExchange(props.data))
    },[]);


    return (
        <div>
            <Dialog
                open={open}
                onClose={closeDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"آیا از پاک کردن این آیتم اطمینان دارید؟"}</DialogTitle>
                <DialogActions>
                    <Button onClick={closeDialog} color="primary">
                        خیر
                    </Button>
                    <Button onClick={handleDelete} color="primary" autoFocus>
                        بله
                    </Button>
                </DialogActions>
            </Dialog>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                autoHideDuration={1000}
                open={snackOpen}
                message={<span id="message-id">{snackText}</span>}
                onClose={handleCloseSnackbar}
                action={[
                    <Button key="undo" color="secondary" size="small" onClick={handleCloseSnackbar}>
                        اوکی
                    </Button>
                ]}
            />
            <Grid className={classes.root} container spacing={3} justify="center" direction="row" alignItems="center">
                <Grid container justify="center">
                    <Card className={classes.card}>
                        <Form className="text-center" onFetchInit={() => console.log('init')} onFetchEnd={(json) => {
                            setSnackText('ساخته شد');
                            setSnackOpen(true);
                            dispatch(addExchange(json));
                        }} validate={validate} url='admin-panel/exchange/create'
                              init={{method: 'POST'}}>
                            <CardContent>
                                <TextField id="outlined-basic" name='unitNamePersian' label="نام ارز به فارسی"/>
                                <TextField id="outlined-basic" name='unitNameTurkish' label="نام ارز به ترکی"/>
                                <TextField type='number' id="outlined-basic" name='price' label="قیمت ارز"/>
                            </CardContent>
                            <CardActions>
                                <Button variant="contained" color="secondary" submit="true">
                                    ساخت
                                </Button>
                            </CardActions>
                        </Form>
                    </Card>
                </Grid>
                {exchanges.length !== 0 &&
                    <Card className={classes.card} style={{'backgroundColor': 'orange'}}>
                    <CardContent>
                        <Grid item xs={12} container justify={"space-evenly"}>
                            {exchanges.map((i)=>{
                                return <Grid item key={i.id}>
                                    <Card className={classes.card}>
                                        <Form className="text-center" notValidateOnStart onFetchInit={() => console.log('init')} onFetchEnd={() => {
                                            setSnackText('آپدیت شد');
                                            setSnackOpen(true)
                                        }} validate={validate} url={`admin-panel/exchange/update/${i.id}`}
                                              init={{method: 'POST'}}>
                                            <CardContent>
                                                <TextField id="outlined-basic" name='unitNamePersian' value={i.unitNamePersian}
                                                           label="نام ارز به فارسی"/>
                                                <TextField id="outlined-basic" name='unitNameTurkish' value={i.unitNameTurkish}
                                                           label="نام ارز به ترکی"/>
                                                <TextField id="outlined-basic" name='price' label="قیمت ارز"
                                                           value={i.price}
                                                           InputProps={{
                                                               startAdornment: (
                                                                   <InputAdornment position="start">
                                                                       <AttachMoney color="primary"/>
                                                                   </InputAdornment>
                                                               ),
                                                           }}/>
                                            </CardContent>
                                            <CardActions>
                                                <Button className="ml-2" variant="contained" color="primary" submit="true">
                                                    آپدیت
                                                </Button>
                                                <Fab color="secondary" aria-label="edit" onClick={()=>{
                                                    deleteIdRef.current = i.id;
                                                    setOpen(true)
                                                }}>
                                                    <DeleteIcon/>
                                                </Fab>
                                                <UploaderComponent className="mr-auto exchange-image" onResponse={(data)=>{
                                                    setUpdater(new Date().getUTCMilliseconds());
                                                }} accept="image/*" url={Env.domain + '/admin-panel/exchange/upload'} body={{id : i.id}} onUploadPercent={(percent)=>{
                                                    console.log(percent);}}>
                                                    <ReactImageFallback
                                                        src={`img/exchange_pics/${i.id}.png?${new Date().getUTCMilliseconds()}`}
                                                        fallbackImage="img/exchange_pics/default.png"
                                                        initialImage="loader.gif"
                                                        alt="cool image should be here"
                                                        width={50}/>
                                                </UploaderComponent>
                                            </CardActions>
                                        </Form>
                                    </Card>
                                </Grid>
                            })}
                        </Grid>
                    </CardContent>
                </Card>
                }
            </Grid>
        </div>
    )
}
