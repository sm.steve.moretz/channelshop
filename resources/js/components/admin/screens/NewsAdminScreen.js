import React, {useRef, useState} from 'react';
import {makeStyles} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add'
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import fetchServer from "../../../modules/fetchServer";
const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 150,
        },
        flexGrow: 1,
    },
    card: {
        maxWidth: 345,
        width : 345
    },
    media: {
        height: 140,
    }
}));
export default function CSSGrid(props) {
    const [open, setOpen] = useState(false);
    const closeDialog = () => {
        setOpen(false);
    };

    const deleteId = useRef();

    const classes = useStyles();
    return (
        <div>
            <Dialog
                open={open}
                onClose={closeDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"آیا از پاک کردن این خبر اطمینان دارید؟"}</DialogTitle>
                <DialogActions>
                    <Button onClick={closeDialog} color="primary">
                        خیر
                    </Button>
                    <Button onClick={async () => {
                        closeDialog();
                        await fetchServer('admin-panel/news/delete/' + deleteId.current,'POST');
                        window.location.reload();
                    }} color="primary" autoFocus>
                        بله
                    </Button>
                </DialogActions>
            </Dialog>
            <Fab color="secondary" style={{position : 'fixed',left : 0,bottom : 0,margin : 16}} onClick={()=>{window.location.href='news/create'}}><AddIcon/></Fab>
            <Grid className={classes.root} container spacing={3} justify="center" direction="row" alignItems="center">
                {props.news.length === 0 && <h6>هیچ خبری ثبت نشده است در صورت ثبت کردن صفحه را رفرش کنید</h6>}
                {props.news.map((news)=>{
                return (
                    <Grid className="m-3">
                        <Card className={classes.card}>
                            <CardActionArea>
                                <CardMedia
                                    className={classes.media}
                                    image={`/img/news/${news.id}.png`}
                                    title="News Image"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {news.title}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {news.summary}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                            <CardActions>
                                <Button size="small" color="primary" href={'news/update/'+news.id}>
                                    ادیت
                                </Button>
                                <Button size="small" color="primary" onClick={()=>{
                                    deleteId.current = news.id;
                                    setOpen(true);
                                }}>
                                    پاک کردن
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>)
            })}
            </Grid>
        </div>
    )
}
