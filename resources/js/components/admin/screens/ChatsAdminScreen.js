import React, {useEffect, useRef, useState} from 'react';
import {Card, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {addAllChatLists, addAllChatMessages, addChatMessage} from "../redux/chatsAction";
import List from "@material-ui/core/List";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import 'react-chat-widget/lib/styles.css';
import ChatWindow, {MessageChatItem} from "../../../modules/ChatWindow";
import fetchServer from "../../../modules/fetchServer";
import Ws from "../../../modules/WS";
import WSWrapper from "../../../modules/WSWrapper";

const useStyles = makeStyles(theme => ({}));

export default function CSSGrid(props) {
    // console.log(props);
    const chats = useSelector(state => state.chats.chatList);
    const chatMessages = useSelector(state => state.chats.chatMessages);
    const classes = useStyles();
    const dispatch = useDispatch();

    const [chatterName, setChatterName] = useState();
    const [connectedToWS, setConnectedToWS] = useState(false);

    const currentOtherUserId = useRef();

    const chatWindow = useRef();

    useEffect(() => {
        dispatch(addAllChatLists(props.chats));
        WSWrapper.addListener((payload)=>{
            if(currentOtherUserId.current == payload.from_id || currentOtherUserId.current == payload.to_id){
                dispatch(addChatMessage({...payload,fromUser : undefined}));
            }
        })
    }, []);


    useEffect(() => {
        setConnectedToWS(!!Ws.userId);
    }, [Ws.userId]);


    const onChatHandler = async (id) => {
        try{
            var res = await fetchServer('/admin-panel/chats/chat/' + id, 'POST');
            var result = await res.json();
            setChatterName(result.user.name);
            currentOtherUserId.current = result.user.id;
            dispatch(addAllChatMessages(result.messages));
            chatWindow.current.open()
        }catch (e) {console.log(e);}
    };


    return (
        <div>
            <ChatWindow onSubmit={(text)=>{
                if(Ws.isAvailable()){
                    Ws.send(currentOtherUserId.current,{'msg':text},false);
                    chatWindow.current.clearInput();
                }
            }} ref={chatWindow} title='چت با کاربر ها' subtitle={chatterName} disabled={!chatterName || !connectedToWS}>
                {chatMessages.map((item)=>{
                    return <MessageChatItem key={item.id} message={item.message} date={item.time} isMe={item.from_id === 1}/>
                })}
            </ChatWindow>
            <List className="text-center">
                {chats.length === 0 &&
                <Card>
                    <CardContent>
                        <h5>هیچ چت پاسخ نداده ای وجود ندارد</h5>
                    </CardContent>
                </Card>
                }
                {chats.map((item) => {
                    return <Button key={item.id} onClick={()=>{onChatHandler(item.fromUser.id)}}>
                        <ListItem alignItems="flex-start" onClick={() => {
                        }} style={{border: '1px solid gray', borderRightColor: 'green',maxWidth : 300}}>
                            <ListItemAvatar>
                                <Avatar alt={item.fromUser.name}/>
                            </ListItemAvatar>
                            <ListItemText
                                className="text-right"
                                primary={
                                    <React.Fragment>
                                        <Typography
                                            component="div"
                                            variant="body1"
                                            color="textPrimary">
                                            {item.fromUser.name}
                                        </Typography>
                                        <Typography
                                            component="div"
                                            variant="subtitle2"
                                            color="textPrimary" noWrap aria-rowcount={3}>
                                            {item.message}
                                        </Typography>
                                        <Typography
                                            component="div"
                                            variant="caption"
                                            color="textPrimary">
                                            تاریخ : {item.time}
                                        </Typography>
                                    </React.Fragment>
                                }
                            />
                            <div>
                            </div>
                        </ListItem>
                    </Button>
                })}
            </List>
        </div>
    )
}
