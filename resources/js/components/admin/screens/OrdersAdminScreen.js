import React, {useEffect} from 'react';
import {Card, CardHeader, makeStyles} from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import ReactImageFallback from "react-image-fallback";
import UploaderComponent from "../../../modules/UploaderComponent";
import {Env} from "../../../env";
import {useDispatch, useSelector} from "react-redux";
import CardContent from "@material-ui/core/CardContent";
import {addAllOrders, removeOrder} from "../redux/orderAction";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        '& .MuiList-root': {
            textCenter: true
        },
    },
    inline: {
        display: 'inline',
    },
}));

export default function CSSGrid(props) {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.orders.orders);
    const classes = useStyles();
    useEffect(() => {
        dispatch(addAllOrders(props.order));
    }, []);
    return (
        <List className="text-center">
            {orders.length === 0 &&
                <Card>
                    <CardContent>
                        <h5>هیچ سفارش تایید نشده ای وجود ندارد</h5>
                    </CardContent>
                </Card>
            }
            {orders.map((item) => {
                return <UploaderComponent key={item.id} className="mr-auto exchange-image" onResponse={(data)=>{
                    dispatch(removeOrder(item.id));
                }} accept="image/*" url={Env.domain + '/admin-panel/orders/uploadConfirm/'+item.id} body={{id : item.id}} onUploadPercent={(percent)=>{}}>
                    <Button>
                        <ListItem alignItems="flex-start" onClick={() => {
                        }} style={{border: '1px solid gray', borderRightColor: 'green'}}>
                            <ListItemAvatar>
                                <Avatar alt={item.user.name}/>
                            </ListItemAvatar>
                            <ListItemText
                                className="text-right"
                                primary={
                                    <React.Fragment>
                                        <Typography
                                            component="div"
                                            variant="body1"
                                            color="textPrimary">
                                            {item.user.name}
                                        </Typography>
                                        <Typography
                                            component="div"
                                            variant="body2"
                                            color="textPrimary">
                                            {'از ' + item.from_price.toFixed(2) + ' ' + item.from_unit + ' به ' + item.price.toFixed(2) + ' ' + item.to_unit}
                                        </Typography>
                                        <Typography
                                            component="div"
                                            variant="subtitle1"
                                            color="textPrimary">
                                            تاریخ : {item.time}
                                        </Typography>
                                        <Typography
                                            component="span"
                                            variant="subtitle1"
                                            color="textPrimary">
                                            شماره کارت
                                        </Typography>
                                        {` — ${item.user.bankcard}`}
                                        <div>
                                            <ReactImageFallback
                                                src={`/img/orders/${item.id}-request.png?${new Date().getUTCMilliseconds()}`}
                                                fallbackImage="/img/exchange_pics/default.png"
                                                initialImage="loader.gif"
                                                alt="cool image should be here"
                                                className="img-fluid"/>
                                        </div>
                                    </React.Fragment>
                                }
                            />
                            <div>
                            </div>
                        </ListItem>
                    </Button>
                </UploaderComponent>
            })}
        </List>
    )
}
