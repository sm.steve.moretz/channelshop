import React from 'react';
import {useSelector} from "react-redux";
import DropDownMenu from "./components/DropDownMenu";
import NotificationDropdown from "./components/lists/NotificationDropdown";
import MessageDropdownItem from "./components/items/MessageDropdownItem";
import NotificationDropdownItem from "./components/items/NotificationDropdownItem";

const NavDropDownNotificationsMenu = props => {
    const notifications = useSelector(state => state.admin.notifications);
    const notificationCount = useSelector(state => state.admin.notificationCount);

    if(notificationCount <= 0){
        return <div/>
    }

    return (
        <DropDownMenu badge={notificationCount} icon='fa-bell-o'>
            <NotificationDropdown footer='مشاهده ی همه ی پیام ها' {...props}>
                {notifications.map((notification) => {
                    return (
                        <NotificationDropdownItem {...notification}/>
                    )
                })}
            </NotificationDropdown>
        </DropDownMenu>
    )
};

export default NavDropDownNotificationsMenu;
