import React from 'react';
import {useSelector} from "react-redux";
import DropDownMenu from "./components/DropDownMenu";
import NotificationDropdown from "./components/lists/NotificationDropdown";
import MessageDropdownItem from "./components/items/MessageDropdownItem";

const NavDropDownMessageMenu = props => {
    const messages = useSelector(state => state.admin.messages);
    const messagesCount = useSelector(state => state.admin.messageCount);

    if(messagesCount <= 0){
        return <div/>
    }

    return (
        <DropDownMenu badge={messagesCount} icon='fa-comments-o'>
            <NotificationDropdown footer='مشاهده ی همه ی پیام ها' {...props}>
                {messages.map((message) => {
                    return (
                        <MessageDropdownItem {...message}/>
                    )
                })}
            </NotificationDropdown>
        </DropDownMenu>
    )
};

export default NavDropDownMessageMenu;
