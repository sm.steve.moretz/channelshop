export const ADD_ALL_CHAT_LISTS = 'ADD_ALL_CHAT_LISTS';
export const ADD_ALL_CHAT_MESSAGES = 'ADD_ALL_CHAT_MESSAGES';
export const ADD_CHAT_MESSAGE = 'ADD_CHAT_MESSAGE';

export const addChatMessage = (message) => {
    return{
        type: ADD_CHAT_MESSAGE,
        message
    };
};
export const addAllChatMessages = (messages) => {
    return{
        type: ADD_ALL_CHAT_MESSAGES,
        messages
    };
};

export const ADD_CHAT_LIST = 'ADD_CHAT_LIST';

export const addChatList = (chatList) => {
    return{
        type: ADD_CHAT_LIST,
        chatList
    };
};

export const addAllChatLists = (chatList) => {
    return{
        type: ADD_ALL_CHAT_LISTS,
        chatList
    };
};
