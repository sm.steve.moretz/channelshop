import {ADD_ALL_EXCHANGE, ADD_EXCHANGE, REMOVE_EXCHANGE} from "./exchangeAction";

const initialState = {
    listOfExchanges : []
};

export default (state = initialState,action) => {
    switch (action.type) {
        case ADD_ALL_EXCHANGE:
            return {...state,listOfExchanges : action.listOfExchange};
        case ADD_EXCHANGE:
            return {...state,listOfExchanges : [...state.listOfExchanges,action.exchange]};
        case REMOVE_EXCHANGE:
            return {...state,listOfExchanges : state.listOfExchanges.filter((item)=>{return item.id != action.id})}
    }
    return state;
}
