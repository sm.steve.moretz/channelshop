export const ADD_ALL_EXCHANGE = 'ADD_ALL_EXCHANGE';
export const ADD_EXCHANGE = 'ADD_EXCHANGE';
export const REMOVE_EXCHANGE = 'REMOVE_EXCHANGE';

export const removeExchange = (id) => {
    return{
        type: REMOVE_EXCHANGE,
        id : id
    };
};
export const addExchange = (exchange) => {
    return{
        type: ADD_EXCHANGE,
        exchange
    };
};
export const addAllExchange = (listOfExchange) => {
    return{
        type: ADD_ALL_EXCHANGE,
        listOfExchange
    };
};
