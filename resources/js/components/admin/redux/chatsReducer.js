import {ADD_ALL_CHAT_LISTS, ADD_ALL_CHAT_MESSAGES, ADD_CHAT_LIST, ADD_CHAT_MESSAGE} from "./chatsAction";

const initialState = {
    chatList : [],
    chatMessages : []
};

export default (state = initialState,action) => {
    switch (action.type) {
        case ADD_CHAT_MESSAGE:
            return {...state,chatMessages : [...state.chatMessages,action.message]};
        case ADD_CHAT_LIST:
            return {...state,chatList : [...state.chatList.filter((item)=>{return action.chatList.fromUser.id !== item.fromUser.id}),action.chatList]};
        case ADD_ALL_CHAT_LISTS:
            return {...state,chatList : action.chatList};
        case ADD_ALL_CHAT_MESSAGES:
            return {...state,chatMessages : action.messages};
    }
    return state;
}
