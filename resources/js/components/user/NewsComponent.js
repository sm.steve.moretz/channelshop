import PropTypes from 'prop-types'
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import {red} from '@material-ui/core/colors';
import {CardActions} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import CardActionArea from "@material-ui/core/CardActionArea";
import Colors from "../constants/Colors";

const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: 345,
        minWidth : 345,
        backgroundColor : 'rgba(246,246,246,0.88)',
        cursor : 'pointer',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

export default function RecipeReviewCard(props) {
    const classes = useStyles();
    return (
        <Card className={{...classes.card,...props.classes}} elevation={10} {...props}>
            {
                props.html && (
                    <Typography style={{color : Colors.accent,textAlign : 'center',margin : 10}} variant='h4'>{props.title}</Typography>
                )
            }
            <CardActionArea>
            <CardHeader
                classes={{
                    content :{
                        backgroundColor : 'red'
                    }
                }}
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar} style={{color : Colors.accent,backgroundColor : Colors.primary}}>
                        A
                    </Avatar>
                }
                title={!props.html && props.title}
                subheader={props.date}
            />
            <CardMedia
                className={classes.media}
                image={props.image}
                title={props.title}
            />
            <CardContent>
                <Typography variant="body2" color="textSecondary" style={props.html ? {textAlign : 'center'}:{textAlign : 'center',overflowY : 'scroll',overflowX : 'hidden',textOverflow : 'ellipsis',maxHeight : 100}}>
                    {props.summary}
                </Typography>
            </CardContent>
            </CardActionArea>
            {props.html ? (
                <div dangerouslySetInnerHTML={{__html : props.html}} style={{marginRight : 20,marginLeft : 20}}></div>
            ):(
                <CardActions>
                    <Button color="primary" href={props.link}>Read More...</Button>
                </CardActions>
            )}
        </Card>
    );
}

RecipeReviewCard.propTypes = {
  date: PropTypes.any,
  image: PropTypes.any,
  link: PropTypes.any,
  summary: PropTypes.any,
  title: PropTypes.any
}
