import {ADD_MESSAGE, ADD_NOTIFICATION} from "../actions/adminAction";
import React from "react";

const initialState = {
    notifications :[],
    messages : [],
    messageCount : 0,
    notificationCount : 0

};

export default (state = initialState,action) => {
    switch (action.type) {
        case ADD_NOTIFICATION:
            if(state.notifications.length >= 6){state.notifications.splice(0,1)}
            return {...state,notificationCount : state.notificationCount + 1,notifications : [...state.notifications,action.notification]};
        case ADD_MESSAGE:
            if(state.messages.length >= 5){state.messages.splice(0,1)}
            return {...state,messageCount : state.messageCount+1,messages : [...state.messages,action.message]};
    }
    return state;
}
