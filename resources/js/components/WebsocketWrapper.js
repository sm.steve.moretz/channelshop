import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import {addChatList} from "./admin/redux/chatsAction";
import WSWrapper from "../modules/WSWrapper";
import {addMessage, addNotification} from "./redux/actions/adminAction";

const WebsocketWrapper = props => {

    const dispatch = useDispatch();

    useEffect(() => {
        WSWrapper.reconnect();
        WSWrapper.addListener((payload)=>{
            console.log(payload);
            if(payload.method == 'chat'){
                payload = {...payload,method : undefined};
                if(payload.from_id !== Env.userId){
                    dispatch(addChatList(payload));
                    dispatch(addMessage(payload.message,'red',payload.created_at));
                }
                console.log(payload);
            }else if(payload.method == 'order'){
                payload = {...payload,method : undefined};
                dispatch(addNotification('سفارش جدید','/admin-panel/orders',payload.order.time))
            }
        });
    }, []);

    return (props.children)
};

export default WebsocketWrapper;
