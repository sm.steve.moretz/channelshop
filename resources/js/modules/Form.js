import PropTypes from 'prop-types'
import React, {forwardRef, useEffect, useImperativeHandle, useReducer, useRef, useState} from 'react';
import {isString} from "lodash";
import {searchAllChildren} from "./utils";
import {Env} from "../env";

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
        const updatedValues = {
            ...state.inputValues,
            [action.name]: action.value
        };
        const updatedValidities = {
            ...state.inputValidities,
            [action.name]: action.isValid
        };
        let updatedFormIsValid = true;
        for (const key in updatedValidities) {
            updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
        }
        return {
            formIsValid: updatedFormIsValid,
            inputValidities: updatedValidities,
            inputValues: updatedValues
        };
    }
    return state;
};
var _isMounted = false;
const Form = forwardRef((props, ref) => {
    const arr = useRef([]);
    let children;
    let errorHelper = {};
    let inputsHelper = [];
    let initialValues = [];

    useImperativeHandle(ref, () => ({
        getData() {
            return formState
        },
        clearInput(name) {
            textChangeHandler(name, '');
        },
        async submit() {
            await submitHandler();
        }
    }));

    //region no need to see
    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {...initialValues}, inputValidities: {}, formIsValid: false
    });
    //endregion

    const [errors, setErrors] = useState({});

    // if(errorHelper){if(errorHelper.length === errors.length)errorHelper = null;}

    useEffect(() => {
        _isMounted = true;
        if (errorHelper.length !== 0) {
            setErrors(errorHelper);
            errorHelper = {};
        }
        if (inputsHelper.length !== 0) {
            inputsHelper.forEach(input => {
                dispatchFormState(input);
            });
            inputsHelper = [];
        }
        return () => {
            _isMounted = false;
        }
    }, []);

    const textChangeHandler = (text, name) => {
        let isValid = props.validate(name, text, formState.inputValidities[name]);
        setErrors({...errors, [name]: undefined});
        errorHelper = {...errorHelper , [name] : undefined};
        if (isString(isValid)) {
            children = null;
            if (!_isMounted) {
                errorHelper = {...errorHelper, [name]: isValid};
                if (_isMounted) {
                    setErrors({...errors, ...errorHelper});
                }
            } else {
                errorHelper = {...errorHelper, [name]: isValid};
                if (errors.length >= errorHelper.length) {
                    errorHelper = {};
                }
                setErrors({...errors, ...errorHelper});
            }
            isValid = false;
        }
        if (_isMounted) {
            dispatchFormState({
                type: FORM_INPUT_UPDATE,
                value: text,
                isValid: isValid,
                name: name
            });
        } else {
            inputsHelper = [...inputsHelper, {
                type: FORM_INPUT_UPDATE,
                value: text,
                isValid: isValid,
                name: name
            }];
        }
    };

    const submitHandler = async () => {
        if (!formState.formIsValid) {
            return;
        }
        let body = props.init && props.init.body;
        if (!body) {
            body = {}
        }
        if (props.onFetchInit) props.onFetchInit();
        const response = await fetch(Env.domain +'/'+ props.url, {
            credentials: "same-origin",
            ...props.init,
            headers: {
                'X-CSRF-TOKEN': Env.csrf,
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/json',
                ...props.init.headers
            },
            body: JSON.stringify({...body, ...formState.inputValues})
        });
        // console.log(await response.text());
        var json = await response.json();
        console.log(json);
        if (json.errors) {
            children = null;
            setErrors(json.errors);
        } else {
            if (props.onFetchEnd) props.onFetchEnd(json);
            children = null;
            setErrors({});
        }
    };


    if (!children) {
        children = searchAllChildren(props.children, 'TextField', (node) => {
            var name = node.props.name;
            if(props.notValidateOnStart) arr.current = [...arr.current, {name : name,value : node.props.value || ''}];
            node.props = {
                ...node.props, value: formState.inputValues[name] || '', onChange: (event) => {
                    textChangeHandler(event.target.value, node.props.name)
                }, error: !!errors[name], helperText: errors[name]
            }
        });
        children = searchAllChildren(children, 'Button', node => {
            if (node.props.submit) {
                node.props = {...node.props, onClick: submitHandler}
            }
        });
    }

    useEffect(() => {
        arr.current.forEach((item) => {
            textChangeHandler(item.value, item.name);
        });
    }, []);

    return <div>
        {children && children}
    </div>
});


export default Form;

Form.propTypes = {
  children: PropTypes.any,
  init: PropTypes.any,
  name: PropTypes.any,
  notValidateOnStart: PropTypes.any,
  onFetchEnd: PropTypes.any,
  onFetchInit: PropTypes.any,
  submit: PropTypes.any,
  url: PropTypes.any,
  validate: PropTypes.any,
  value: PropTypes.any
}
