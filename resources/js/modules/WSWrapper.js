import {Component} from "react";
import Ws from "./WS";

export default class WSWrapper extends Component {
    static listeners = [];
    static addListener(listener){
        WSWrapper.listeners = [...WSWrapper.listeners,listener];
    }

    componentDidMount() {
        // if(!Ws.isAvailable()) WSWrapper.reconnect();
    }

    static reconnect(navigation){
        if(!Ws.isAvailable()){
            console.log('reconnecct');
            Ws.connect((u)=>{
            },(e)=>{
                console.log('error : ' + e);
                // alert(`Error ${e}!`);
            },(msg)=>{
                // console.log(msg);
                if(msg.payload) {
                    WSWrapper.listeners.forEach((item)=>{
                        item(msg.payload)
                    });
                }
            },navigation);
        }
    }

    render() {
        return this.props.children;
    }
}
