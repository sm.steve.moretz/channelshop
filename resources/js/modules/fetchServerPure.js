import {Env} from "../env";

export default async (url,method = 'GET',body,headers = {},props) => {
    if (method && method.toUpperCase() === 'GET' && body) {
        if (!url.endsWith('?')) url += '?';
        var index = 0;
        for (var p in body) {
            if (index++ !== 0) url += '&';
            url += p + '=' + body[p];
        }
        body  = undefined;
    }
    return await fetch(Env.domain +'/'+ url, {
        credentials: "include",
        ...props,
        method : method,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
            ...headers
        },
        body: body ? JSON.stringify({...body}) : undefined
    });
}
