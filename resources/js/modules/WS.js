import {Env} from "../env";

export default class Ws{
    static userId;
    static conn;
    static async sendToTopic(topic,payload,persist){
        await Ws.conn.send(JSON.stringify({command: "messageToTopic", topic: topic,persist, ...payload}));
    }
    static async send(userId, payload,persist) {
        await Ws.conn.send(JSON.stringify({command: "message", to: userId,persist, ...payload}));
    }
    static async sendCustom(command,data){
        await Ws.conn.send(JSON.stringify({command: command, ...data}));
    }

    static isAvailable(){
        return !!Ws.conn && !!Ws.userId;
    }
    // static async subscribe(topic){
    //     if(!Ws.conn){
    //         throw new Error('Not connected to subscribe');
    //     }
    //     Ws.conn.send(JSON.stringify({command: "subscribe", topic: topic}));
    // }
    static connect(authCallback,errorCallback,messageCallback,url = Env.wsUrl,ratchetToken=Env.ratchetToken) {
        if(!Ws.conn){
            Ws.conn = new WebSocket(url);
        }
        Ws.conn.onerror = function (e) {
            if(errorCallback){errorCallback('connection')}
            console.log('error occured');
            Ws.conn = undefined;
            Ws.userId = undefined;
            // console.log(e);
        };
        Ws.conn.onopen = function (e) {
            // console.log("Connection established!");
            if(!Ws.userId){
                Ws.conn.send(JSON.stringify({command: "auth", token: ratchetToken}));
            }
            // conn.send(JSON.stringify({command: "register", userId: 9}));
            // conn.send(JSON.stringify({command: "message", from:"9", to: "1", message: "Hello"}));
        };
        Ws.conn.onmessage = async function (e) {
            // console.log(e.data);
            let parse;
            try{
                parse = await JSON.parse(e.data);
            }catch (e) {
                parse = {}
            }
            if(parse.Authenticated === false){
                // navigation.navigate('Auth',{screen : 'Main'});
                console.log('Not Authenticated');
                if(errorCallback){errorCallback('auth')}
                return ;
            }else if(parse.Authenticated){
                // console.log('Authenticated');
                authCallback(parse.userId);
                Ws.userId = parse.userId;
                return ;
            }
            if (parse.notification) {//example conn.send(JSON.stringify({command: "message", to: 6, body: "سریع",title: "پیام از یارو",color : 'yellow',notification :true}));
                // await Notifications.presentLocalNotificationAsync({
                //     title: parse.title ? parse.title : 'New Message',
                //     body: parse.body,
                //     android: {
                //         priority: 'max',
                //         vibrate: [0, 250, 250, 250],
                //         color: parse.color ? parse.color : '#FF0000',
                //     },
                // });
                return ;
            }
            if(parse){
                messageCallback(parse);
            }else{
                messageCallback(e.data);
            }
        };
    }
}
