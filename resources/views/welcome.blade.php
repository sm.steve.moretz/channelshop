<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta name='apple-mobile-web-app-capable' content='yes' />
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/chat.css')}}">
</head>
<style>
    footer{
        border: 1px solid #d2ac50;
    }
    *:visited {
        outline: none;
        border: transparent;
    }
    button:focus { outline: none; }
    .cursor-pointer{
        cursor: pointer;
    }
    .uploader{
        transition: .5s;
    }
    .uploader:hover{
        transform: translateY(-10px) rotate3d(5,5,50,10deg);
    }
</style>
<body class="bg-dark">
@react([ 'style'=>'width:100%;','id' => 'main', 'props' => []])
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
