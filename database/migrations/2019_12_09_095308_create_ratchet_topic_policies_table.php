<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatchetTopicPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratchet_topic_policies', function (Blueprint $table) {
            $table->unsignedInteger('topic')->default(null);
            $table->unsignedInteger('blocked')->default(null);
            $table->unsignedInteger('allowed')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratchet_topic_policies');
    }
}
